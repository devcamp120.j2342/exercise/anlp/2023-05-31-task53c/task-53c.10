import models.Rectangle;
import models.Triangle;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle(10,20,"red");
        Rectangle rectangle2 = new Rectangle(30,20,"blue");
        System.out.println(rectangle1);
        System.out.println(rectangle2);

        Triangle triangle1 = new Triangle(10, 20, "red");
        Triangle triangle2 = new Triangle(5, 10, "white");
        System.out.println(triangle1);
        System.out.println(triangle2);
    }
}
