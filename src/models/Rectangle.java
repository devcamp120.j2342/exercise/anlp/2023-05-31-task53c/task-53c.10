package models;

public class Rectangle extends Shape {
    private int length;
    private int width;

    public Rectangle() {
    }

    public Rectangle(int length, int width) {
        this.length = length;
        this.width = width;
    }
    public Rectangle(int length, int width,String color) {
        super(color);
        this.length = length;
        this.width = width;
    }

    public int getLength() {
        return this.length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
    public double getArea(){
        return this.width * this.length;
    }


    @Override
    public String toString() {
        return "Rectangle[" + " length='" + getLength() + "', width='" + getWidth() + "', color='" + super.getColor() + "', area='" + getArea() + "']" ;
    }

}
