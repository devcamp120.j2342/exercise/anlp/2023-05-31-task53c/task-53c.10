package models;

public class Triangle extends Shape {
    private int base;
    private int height;

    public Triangle() {
    }

    public Triangle(int base, int height) {
        this.base = base;
        this.height = height;
    }
    public Triangle(int base, int height, String color) {
        super(color);
        this.base = base;
        this.height = height;
    }
    public int getBase() {
        return this.base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    public double getArea(){
        return this.base * this.height;
    }


    @Override
    public String toString() {
        return "Triangle[" +
            " base='" + getBase() + "'" +
            ", height='" + getHeight() + "'" +
            ", color='" + super.getColor() + "'" +
            ", area='" + getArea() + "'" +
            "]";
    }

}
