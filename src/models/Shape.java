package models;

public abstract class Shape {
    private String color;

    public Shape(String color) {
        this.color = color;
    }

    public Shape() {
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    @Override
    public String toString(){
        return "Shape[color="+ color + "]";
    }
    public abstract double getArea();
}
